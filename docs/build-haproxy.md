# Build ___haproxy___

## Pre requisites

### env. vars

```bash
export HAPROXY_DIR=$HOME/work/haproxy
export WORK_DIR=/ehc/fs1/adp/fcolas/sources

export PREFIX_BASE=$HOME
export PREFIX_ETC=$PREFIX_BASE/etc
export PREFIX_USR_LOCAL=$PREFIX_BASE/usr/local

# prepare PATH with our own prefix
export PATH=$PREFIX_USR_LOCAL/bin:$PREFIX_USR_LOCAL/sbin:$PREFIX_USR_LOCAL/ssl/bin:$PATH
# prepare LD_LIBRARY_PATH with our own prefixes
export LD_LIBRARY_PATH=$PREFIX_USR_LOCAL/lib:$PREFIX_USR_LOCAL/ssl/lib:$LD_LIBRARY_PATH

# configuration variables
export SITE_NAME=mysite-1
export HTTP_LISTENING_PORT1=1880
export HTTP_LISTENING_PORT2=2880
export HTTPS_LISTENING_PORT1=1443
export HTTPS_LISTENING_PORT2=2443

# for certificates
mkdir -p $PREFIX_ETC/ssl/certs/
mkdir -p $PREFIX_ETC/haproxy/

alias goworkdir='cd $WORK_DIR'
alias gousrlocal='cd $PREFIX_USR_LOCAL'
alias reloadenv='source $HAPROXY_DIR/setenv.sh'

goworkdir

```

## Build ___haproxy___ and dependencies

### PCre

* https://ftp.pcre.org/pub/pcre/pcre-8.43.tar.bz2

```bash
./configure --prefix=$PREFIX_USR_LOCAL
make && make install

```

### ZLib

* https://www.zlib.net/zlib-1.2.11.tar.gz

```bash
./configure --prefix=$PREFIX_USR_LOCAL
make && make install

```

### OpenSSL

* https://www.openssl.org/source/openssl-1.1.1d.tar.gz

```config``` options: https://wiki.openssl.org/index.php/Compilation_and_Installation

```bash
./config --prefix=$PREFIX_USR_LOCAL/ssl shared no-zlib

#-I$PREFIX_USR_LOCAL/include -L$PREFIX_USR_LOCAL/lib
make && make install

```

## ___haproxy___

* make options https://github.com/haproxy/haproxy/blob/master/Makefile

```bash
make TARGET=linux-glibc CPU=native USE_STATIC_PCRE=1 USE_OPENSSL=1 USE_ZLIB=1 SSL_INC=$PREFIX_USR_LOCAL/ssl/include SSL_LIB=$PREFIX_USR_LOCAL/ssl/lib PCREDIR=$PREFIX_USR_LOCAL

```

## Configuration file for testing

```bash
mkdir -p $PREFIX_ETC/haproxy
mkdir -p $PREFIX_ETC/ssl/certs
```

### Mimic HTTP site

* Start HTTP instances

in session 1

```bash
python -m SimpleHTTPServer $HTTP_LISTENING_PORT1
```

to test it
```bash
curl -k http://localhost:${HTTP_LISTENING_PORT1}/
```


in session 2

```bash
python -m SimpleHTTPServer $HTTP_LISTENING_PORT2
```

to test it
```bash
curl -k http://localhost:${HTTP_LISTENING_PORT2}/
```

### Mimic HTTPS site (mysite-1)

* Generate a SSL certificate

```bash
goworkdir
mkdir certs; cd certs

openssl genrsa -out root.key 2048
openssl req -new -key root.key -out ${SITE_NAME}.csr -subj "/C=FR/ST=IDF/L=Paris/O=Test/OU=Test/CN=test.com"
openssl x509 -req -days 3650 -in ${SITE_NAME}.csr -signkey root.key -out ${SITE_NAME}.crt
cat root.key ${SITE_NAME}.crt > ${SITE_NAME}.pem

ls -l ${SITE_NAME}.pem && openssl x509 -in ${SITE_NAME}.pem -text -noout
# copy certificate to /etc/ssl/certs directory
cp -p ${SITE_NAME}.pem $PREFIX_ETC/ssl/certs

```

* Start OpenSSL instances

In session 1

```bash

openssl s_server -accept ${HTTPS_LISTENING_PORT1} -cert $PREFIX_ETC/ssl/certs/${SITE_NAME}.pem -www
```

to test it
```bash
openssl s_client -connect localhost:${HTTPS_LISTENING_PORT1}
# or
curl -k https://localhost:${HTTPS_LISTENING_PORT1}/
```

In session 2

```bash
openssl s_server -accept ${HTTPS_LISTENING_PORT2} -cert $PREFIX_ETC/ssl/certs/${SITE_NAME}.pem -www
```

to test it 

```bash
openssl s_client -connect localhost:${HTTPS_LISTENING_PORT2}
# or
curl -k https://localhost:${HTTPS_LISTENING_PORT2}/
```


### 

### basic RP + Load balancing

vi `$PREFIX_ETC/haproxy/${SITE_NAME}.cfg`

```bash
# HA Proxy Config
global
 daemon
 maxconn 256

defaults
 mode http
 timeout connect 5000ms
 timeout client 50000ms
 timeout server 50000ms

listen stats
 bind *:9999
 stats enable
 stats hide-version
 stats uri /stats
 stats auth admin:admin@123

frontend myApp
 bind *:2080
# acl acl_myApp path_sub myApp
 use_backend myAppBackEnd

backend myAppBackEnd
 balance roundrobin
 option httpchk HEAD /?ping
 # w/ SSL
 #server myAppServer1 127.0.0.1:$HTTPS_LISTENING_PORT1 check ssl verify none
# server myAppServer2 127.0.0.1:$HTTPS_LISTENING_PORT2 check ssl verify none
# w/ HTTP
 server myAppServer1 127.0.0.1:$HTTP_LISTENING_PORT1 check
 server myAppServer1 127.0.0.1:$HTTP_LISTENING_PORT2 check
```

### Start HAProxy

```bash
haproxy -d -V -f $PREFIX_ETC/haproxy/${SITE_NAME}.cfg
```

## Annex

### HAProxy stats

Connect to [http://yourserver:9999/stats](http://yourserver:9999/stats)