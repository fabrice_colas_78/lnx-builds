# Build ___htop___

## Pre requisites

### env. vars

```bash
export HTOP_DIR=$HOME/work/htop
export WORK_DIR=/ehc/fs1/adp/fcolas/sources

export PREFIX_BASE=$HOME
export PREFIX_ETC=$PREFIX_BASE/etc
export PREFIX_USR_LOCAL=$PREFIX_BASE/usr/local

# prepare PATH with our own prefix
export PATH=$PREFIX_USR_LOCAL/bin:$PREFIX_USR_LOCAL/sbin:$PATH
# prepare LD_LIBRARY_PATH with our own prefixes
#export LD_LIBRARY_PATH=$PREFIX_USR_LOCAL/lib:$LD_LIBRARY_PATH


if [ ! -d $WORK_DIR ]; then mkdir -p $WORK_DIR; fi

alias goworkdir='cd $WORK_DIR'
alias gousrlocal='cd $PREFIX_USR_LOCAL'
alias reloadenv='source $HTOP_DIR/setenv.sh'

goworkdir
```

## Build ___htop___ and dependencies

### ZLib

* https://www.zlib.net/zlib-1.2.11.tar.gz

```bash
NCURSES_VERSION=6.0
wget http://invisible-mirror.net/archives/ncurses/ncurses-${NCURSES_VERSION}.tar.gz && tar xzf ncurses-${NCURSES_VERSION}.tar.gz && cd ncurses-${NCURSES_VERSION}

./configure --prefix=$PREFIX_USR_LOCAL
make && make install

```

### htop

```bash
goworkdir

HTOP_VERSION=2.2.0
wget http://hisham.hm/htop/releases/${HTOP_VERSION}/htop-${HTOP_VERSION}.tar.gz && tar xzf htop-${HTOP_VERSION}.tar.gz && cd htop-${HTOP_VERSION}

 ./configure --prefix=$PREFIX_USR_LOCAL
 make && make install

```

## Tests

```bash

```
