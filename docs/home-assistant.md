# Install ___Home Assistant___ on Raspbian

## Python in virtual environment

### Pre requisites

```bash
sudo apt-get install -y python3 python3-venv
```

### Installation

```bash
# Create a virtual environment in your current directory
python3 -m venv homeassistant
# Open the virtual environment
cd homeassistant
# Activate the virtual environment
source bin/activate
# Install Home Assistant
python3 -m pip install homeassistant
# Run Home Assistant
hass --open-ui
```

## Docker

```bash
mkdir -p ~/ha/config
docker run --init -d --name="home-assistant" -e "TZ=Europe/Paris" -v ~/ha/config:/config --net=host homeassistant/raspberrypi3-homeassistant:stable
```
