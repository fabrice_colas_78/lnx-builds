# Build ___mdp___

`mdp` is markdown based presentation tool accessible from the command line

github [https://github.com/visit1985/mdp](https://github.com/visit1985/mdp)

## Preparation

```bash
TOOL_NAME=mdp
export BIN_DIR=$HOME/.local/bin
export WORK_DIR=$HOME/work/$TOOL_NAME
export DEPS_DIR=$WORK_DIR/deps
export NCURSES_DIR=$DEPS_DIR/ncurses

export MAX_PARALLEL_COMPIL=4
export NCURSES_VERSION=6.3

echo "Bin Dir    : $BIN_DIR"
echo "Work Dir   : $WORK_DIR"
echo "Deps Dir   : $DEPS_DIR"
echo "NCurses Dir: $NCURSES_DIR"
echo "NCurses Ver: $NCURSES_VERSION"

if [ ! -d $WORK_DIR ]; then mkdir -p $WORK_DIR; fi

alias goworkdir='cd $WORK_DIR'
goworkdir
```

## Dependencies

Dependencies are:

* Ncurses

## nCurses compilation

```bash
pushd .
goworkdir
if [ -d ncurses-${NCURSES_VERSION} ]; then rm -rf ncurses-${NCURSES_VERSION}; fi
wget http://invisible-mirror.net/archives/ncurses/ncurses-${NCURSES_VERSION}.tar.gz
tar xzf ncurses-${NCURSES_VERSION}.tar.gz && cd ncurses-${NCURSES_VERSION}
./configure --prefix=$NCURSES_DIR --enable-widec && make clean && make -j${MAX_PARALLEL_COMPIL} && make install
ls -l lib
popd

```

## `mdp` compilation

```bash
pushd .
goworkdir
git clone https://github.com/visit1985/mdp.git
cd mdp
CPPFLAGS="-I${NCURSES_DIR}/include -I${NCURSES_DIR}/include/ncursesw" \
LDFLAGS="-s -L${NCURSES_DIR}/lib" \
make

cp -p mdp $BIN_DIR

# Building a test presentation
cat >pres-test.md <<EOF
%title: Test presentation
%author: ${USER}
%date: $(date +"%Y-%m-%d")

# Slide 1

## Basic controls

next slide: Enter, Space, Page Down, j, l, Down Arrow, Right Arrow

previous slide: Backspace, Page Up, h, k, Up Arrow, Left Arrow

quit q reload r slide N 1..9 first slide Home, g last slide End, G

## Chapter 1

Content of \`chapter 1\`

~~~
This is a code block
~~~

----

# Slide 2

## Chapter 2

Content of \`chapter 2\`

~~~
This is a another code block
~~~


EOF
mdp pres-test.md

popd

```
