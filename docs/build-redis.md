# Build ___redis___

* https://redis.io/docs/getting-started/installation/install-redis-from-source/

## Preparation

```bash
export WORK_DIR=$HOME/work/redis
export REDIS_DIR=$HOME/.local # .../bin/redis

export MAX_PARALLEL_COMPIL=4

if [ ! -d $WORK_DIR ]; then mkdir -p $WORK_DIR; fi

alias goworkdir='cd $WORK_DIR'
goworkdir

```

## Prerequisites

```bash

```

## Dependencies

Dependencies are:

* None


## Redis compilation

```bash
pushd .
goworkdir
wget https://download.redis.io/redis-stable.tar.gz
tar -xzvf redis-stable.tar.gz
cd redis-stable
make
PREFIX=$REDIS_DIR make install

popd
```

## Test Redis

```
# in a 1st session
resdis-server

# in a 2sd session
redis-cli INCR mycounter
redis-cli INCR mycounter

redis-cli GET mycounter
# should display "2"

```
