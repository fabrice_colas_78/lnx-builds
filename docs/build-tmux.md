# Build ___tmux___

## Preparation

```bash
export WORK_DIR=$HOME/work/tmux
export TMUX_DIR=$HOME/bin/tmux
export UTEMPTER_DIR=$HOME/deps/utempter
export NCURSES_DIR=$HOME/deps/ncurses
export LIBEVENT_DIR=$HOME/deps/libevent

export MAX_PARALLEL_COMPIL=4
export NCURSES_VERSION=6.2
export UTEMPTER_VERSION=1.1.5
export LIBEVENT_VERSION=2.1.11
export TMUX_VERSION=3.0a

if [ ! -d $WORK_DIR ]; then mkdir -p $WORK_DIR; fi

alias goworkdir='cd $WORK_DIR'
goworkdir
```

## Prerequisites

```bash
sudo apt-get install -y bison
```

## Dependencies

Dependencies are:

* uTempter
* Libevent
* Ncurses

## Utempter compilation

```bash
pushd .
goworkdir
wget http://www.mirrorservice.org/sites/download.salixos.org/i486/slackware-14.0/source/a/utempter/libutempter-${UTEMPTER_VERSION}.tar.bz2
tar xjf libutempter-${UTEMPTER_VERSION}.tar.bz2 && cd libutempter-${UTEMPTER_VERSION}
cp -p Makefile Makefile.save; sed -i -e 's/DESTDIR =/DESTDIR = \$(UTEMPTER_DIR)/g' Makefile
make -j${MAX_PARALLEL_COMPIL} && make install
popd
```

## nCurses compilation

```bash
pushd .
goworkdir
wget http://invisible-mirror.net/archives/ncurses/ncurses-${NCURSES_VERSION}.tar.gz
tar xzf ncurses-${NCURSES_VERSION}.tar.gz && cd ncurses-${NCURSES_VERSION}
./configure --prefix=$NCURSES_DIR && make clean && make -j${MAX_PARALLEL_COMPIL} && make install
popd
```

## Libevent compilation

```bash
pushd .
goworkdir
wget https://github.com/libevent/libevent/releases/download/release-${LIBEVENT_VERSION}-stable/libevent-${LIBEVENT_VERSION}-stable.tar.gz
tar xzf libevent-${LIBEVENT_VERSION}-stable.tar.gz && cd libevent-${LIBEVENT_VERSION}-stable
./configure --prefix=$LIBEVENT_DIR && make clean && make -j${MAX_PARALLEL_COMPIL} && make install
popd
```

## Tmux compilation

```bash
pushd .
goworkdir
wget https://github.com/tmux/tmux/releases/download/${TMUX_VERSION}/tmux-${TMUX_VERSION}.tar.gz
tar xzf tmux-${TMUX_VERSION}.tar.gz && cd tmux-${TMUX_VERSION}/
./configure CPPFLAGS="-I$LIBEVENT_DIR/include -I$NCURSES_DIR/include -I$NCURSES_DIR/include/ncurses -I$UTEMPTER_DIR/usr/include" LDFLAGS="-static -L$LIBEVENT_DIR/lib -L$NCURSES_DIR/lib -L$UTEMPTER_DIR/usr/lib" --prefix=$TMUX_DIR && make clean && make -j${MAX_PARALLEL_COMPIL} && make install
$TMUX_DIR/bin/tmux -V
popd
```
