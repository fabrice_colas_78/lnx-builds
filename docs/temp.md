Pre requisites are uTempter, Libevent and Ncurses

Download prerequisites
Download utempter sources
	• http://www.mirrorservice.org/sites/download.salixos.org/i486/slackware-14.0/source/a/utempter/libutempter-1.1.5.tar.bz2 or ftp://ftp.altlinux.org/pub/people/ldv/utempter/libutempter-1.1.6.tar.bz2

Download ncurses sources
	• http://invisible-mirror.net/archives/ncurses/ncurses-6.0.tar.gz

Download Libevent sources
	• https://github.com/libevent/libevent/releases/download/release-2.0.22-stable/libevent-2.0.22-stable.tar.gz

Download TMUX sources
	• https://github.com/tmux/tmux/releases/download/2.1/tmux-2.1.tar.gz


# Env. var. initialisation
export UTEMPTER_DIR=$HOME/deps/utempter
export NCURSES_DIR=$HOME/deps/ncurses
export LIBEVENT_DIR=$HOME/deps/libevent
export TMUX_DIR=$HOME/bin/tmux

# Utempter compilation
mkdir -p ~/install/tmux && cd ~/install/tmux
# Copy  tar.gz file just downloaded before
tar xjf libutempter-1.1.5.tar.bz2 && cd libutempter-1.1.5
cp -p Makefile Makefile.save; sed -i -e 's/DESTDIR =/DESTDIR = \$(UTEMPTER_DIR)/g' Makefile
make -j8 && make install
cd ..
	
# nCurses compilation
mkdir -p ~/install/tmux && cd ~/install/tmux
# Copy  tar.gz file just downloaded before
tar xzf ncurses-6.0.tar.gz && cd ncurses-6.0
./configure --prefix=$NCURSES_DIR && make clean && make -j8 && make install
cd ..

# Libevent compilation
mkdir -p ~/install/tmux && cd ~/install/tmux
# Copy  tar.gz file just downloaded before
tar xzf libevent-2.0.22-stable.tar.gz && cd libevent-2.0.22-stable
./configure --prefix=$LIBEVENT_DIR && make clean && make -j8 && make install
cd ..

# Tmux compilation
mkdir -p ~/install/tmux && cd ~/install/tmux
# Copy  tar.gz file just downloaded before
tar xzf tmux-2.9.tar.gz && cd tmux-2.9/
./configure CPPFLAGS="-I$LIBEVENT_DIR/include -I$NCURSES_DIR/include -I$NCURSES_DIR/include/ncurses -I$UTEMPTER_DIR/usr/include" LDFLAGS="-static -L$LIBEVENT_DIR/lib -L$NCURSES_DIR/lib -L$UTEMPTER_DIR/usr/lib" --prefix=$TMUX_DIR && make clean && make -j8 && make install
$TMUX_DIR/bin/tmux -V
