# Build ___PHP___

## Preparation

```bash
export WORK_DIR=$HOME/install/php
export PHP_DIR=$HOME/bin/php
export LIBXML2_DIR=$HOME/deps/libxml2
export CURL_DIR=$HOME/deps/curl

export MAX_PARALLEL_COMPIL=4

export PHP_VERSION=8.1.2
export LIBXML2_VERSION=2.9.12
export CURL_VERSION=7.81.0

if [ ! -d $WORK_DIR ]; then mkdir -p $WORK_DIR; fi

alias goworkdir='cd $WORK_DIR'
goworkdir
```

## Prerequisites

```bash

```

## Dependencies

Dependencies are:

* Libxml2
* cUrl

## `Libxml2` compilation

```bash
pushd .
goworkdir
wget http://xmlsoft.org/sources/libxml2-${LIBXML2_VERSION}.tar.gz
tar xzf libxml2-${LIBXML2_VERSION}.tar.gz && cd libxml2-${LIBXML2_VERSION}
./configure --prefix=$LIBXML2_DIR && make clean && make -j${MAX_PARALLEL_COMPIL} && make install
popd
```

## `cUrl` compilation

```bash
pushd .
goworkdir
wget https://curl.se/download/curl-${CURL_VERSION}.tar.gz
tar xzf curl-${CURL_VERSION}.tar.gz && cd curl-${CURL_VERSION}/
./configure --prefix=$CURL_DIR --without-ssl && make clean && make -j${MAX_PARALLEL_COMPIL} && make install
popd
```

## `PHP` compilation

```bash
pushd .
goworkdir
wget https://www.php.net/distributions/php-${PHP_VERSION}.tar.gz
tar xzf php-${PHP_VERSION}.tar.gz && cd php-${PHP_VERSION}/

./configure --prefix=$PHP_DIR \
    LIBXML_CFLAGS=-I$LIBXML2_DIR/include/libxml2 LIBXML_LIBS=-L$LIBXML2_DIR/lib \
    CURL_CFLAGS=-I$CURL_DIR/include CURL_LIBS=-L$CURL_DIR/lib --with-curl \
    EXTRA_LIBS="-lxml2 -lcurl" \
    --without-sqlite3 --without-pdo-sqlite
make clean && make -j${MAX_PARALLEL_COMPIL} && make install

$PHP_DIR/bin/php -V
popd
```
