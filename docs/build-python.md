# Build ___pyhton___

## Pre requisites

### env. vars

```bash
export WORK_DIR=/ehc/fs1/adp/fcolas/sources

export PREFIX_BASE=$HOME/.local/python

if [ ! -d $WORK_DIR ]; then mkdir -p $WORK_DIR; fi

alias goworkdir='cd $WORK_DIR'

goworkdir
```

## Build ___python___ and dependencies

### SQLITE3

* https://www.sqlite.org/download.html

```bash
goworkdir

SQLITE_BUILD=3310100
INSTALL_DIR=$HOME/.local/sqlite

wget https://www.sqlite.org/2020/sqlite-autoconf-$SQLITE_BUILD.tar.gz
tar xzf sqlite-autoconf-$SQLITE_BUILD.tar.gz
cd sqlite-autoconf-$SQLITE_BUILD
./configure --prefix=$INSTALL_DIR
make && make install
ls -l $INSTALL_DIR

```

### libffi-devel

e.g. ```libffi-devel-gcc5-5.3.1+r233831-12.1.x86_64```

### Python

```bash
goworkdir

INSTALL_DIR=$HOME/.local/python

git clone https://github.com/python/cpython
cd cpython

# Add SQLITE install dir in setup.py. See https://stackoverflow.com/questions/32779768/python-build-from-source-cannot-build-optional-module-sqlite3
SQLLITE_INCLUDE_DIR=$HOME/.local/sqlite/include
sed -i -e "s|sqlite_inc_paths = \[ '|sqlite_inc_paths = \[ '$SQLLITE_INCLUDE_DIR', \n '|" setup.py

./configure --prefix=$INSTALL_DIR --enable-loadable-sqlite-extensions
make && make install

```

## Tests

```bash
./python -m pip
```
