# Build ___NGinX___

## Preparation

```bash
export WORK_DIR=$HOME/work/nginx
export NGINX_DIR=$HOME/.local/nginx
export DEFAULT_LISTENING_PORT=1180

export MAX_PARALLEL_COMPIL=4
export NGINX_VERSION=1.21.1
export PCRE_VERSION=8.43
export ZLIB_VERSION=1.2.11


if [ ! -d $WORK_DIR ]; then mkdir -p $WORK_DIR; fi

alias goworkdir='cd $WORK_DIR'
alias gonginxdir='cd $NGINX_DIR'
goworkdir

```

## Build ___NGinX___ and dependencies

### PCre

```bash
goworkdir
wget https://ftp.pcre.org/pub/pcre/pcre-${PCRE_VERSION}.tar.bz2 && \
    tar xjf pcre-${PCRE_VERSION}.tar.bz2 && \
    cd pcre-${PCRE_VERSION}
./configure --prefix=$PREFIX_USR_LOCAL && \
    make -j ${MAX_PARALLEL_COMPIL} && make install

```

### ZLib

```bash
goworkdir
wget https://www.zlib.net/zlib-${ZLIB_VERSION}.tar.gz && \
    tar xzf zlib-${ZLIB_VERSION}.tar.gz && \
    cd zlib-${ZLIB_VERSION}
./configure --prefix=$PREFIX_USR_LOCAL && \
    make -j ${MAX_PARALLEL_COMPIL} && make install

```

### Build NGinX

```bash
goworkdir
wget https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && \
    tar xzf nginx-${NGINX_VERSION}.tar.gz && \
    cd nginx-${NGINX_VERSION}

./configure --prefix=$NGINX_DIR \
    --without-http_fastcgi_module \
    --without-http_uwsgi_module \
    --without-http_grpc_module \
    --without-http_scgi_module \
    --without-mail_imap_module \
    --without-mail_pop3_module \
    --with-pcre=../pcre-${PCRE_VERSION} \
    --with-zlib=../zlib-${ZLIB_VERSION}
make -j ${MAX_PARALLEL_COMPIL} && make install

```

## Check installation

```bash
gonginxdir
# Update default listening port
sed -i -e "s/listen.*80/listen       ${DEFAULT_LISTENING_PORT}/" conf/nginx.conf
sbin/nginx -v -t

```
