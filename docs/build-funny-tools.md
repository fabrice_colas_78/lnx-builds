## Prepration

```sh
export WORK_DIR=~/work/unix_funny_tools
mkdir -p $WORK_DIR
cd $WORK_DIR

```

## figlet

```sh
export FIGLET_INSTALLDIR=~/.local/bin
export FIGLET_FONTDIR=~/.local/share/figlet
export INIT_RCFILE=~/.local/tools/initmytools.sh

cd $WORK_DIR

git clone https://github.com/cmatsuoka/figlet.git && cd figlet && make
cp -p ./figlet $FIGLET_INSTALLDIR
mkdir -p $FIGLET_FONTDIR
cp -p fonts/* $FIGLET_FONTDIR

grep FIGLET_FONTDIR $INIT_RCFILE
if [ ! $? == 0 ]; then
    echo Adding FIGLET_FONTDIR in $INIT_RCFILE...
    cp -p $INIT_RCFILE $INIT_RCFILE-$(date +"%Y%m%d-%H%M%S")
    echo "export FIGLET_FONTDIR=$FIGLET_FONTDIR">>$INIT_RCFILE
fi

# test
figlet Test Figlet

```

## boxes

```sh
export BOXES_INSTALLDIR=~/.local/bin

cd $WORK_DIR

git clone https://github.com/ascii-boxes/boxes.git && cd boxes && make static
cp -p ./boxes $BOXES_INSTALLDIR

# test
echo Test Boxes | boxes -d whirly

```

## lolcat

```sh
export LOLCAT_INSTALLDIR=~/.local/bin

cd $WORK_DIR

git clone https://github.com/jaseg/lolcat.git && cd lolcat && make
cp -p ./lolcat $LOLCAT_INSTALLDIR

# test
echo -e "This is a test of lolcat\nThis is a test of lolcat\nThis is a test of lolcat\nThis is a test of lolcat" | lolcat -r

```
