## Portainer on Docker

```bash
PORTAINER_TAG=2.6.3
docker run -d --name="portainer" --restart on-failure -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v $HOME/portainer_data:/data portainer/portainer-ce:${PORTAINER_TAG}

IP=$(ip a | grep "\beth0\b" | grep inet | awk -F" " '{ print $2 }' | awk -F'/' '{ print $1 }') ; echo -e "\nYou can connect at http://$IP:9000\n"

```

## Portainer agent on Docker

```bash
docker run -d -p 9001:9001 --name portainer_agent \
  --restart=always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /var/lib/docker/volumes:/var/lib/docker/volumes \
  portainer/agent

```

## Portainer with Docker-Compose

```bash
IFACE=eth0
IP=$(ip a | grep "\beth0\b" | grep inet | awk -F" " '{ print $2 }' | awk -F'/' '{ print $1 }')

PORTAINER_TAG=2.6.3

CONFIG_DIR=$HOME/portainer

if [ ! -d $CONFIG_DIR ]; then
  mkdir -p ${CONFIG_DIR}
fi

apt install -y docker-compose

DOCKER_COMPOSE_FILE=$CONFIG_DIR/docker-compose.yml
cat >${DOCKER_COMPOSE_FILE} <<EOF
version: '3.3'

services:
  portainer:
    image: portainer/portainer-ce:${PORTAINER_TAG}
    container_name: portainer
    restart: always
    ports:
      - "9000:9000"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ${CONFIG_DIR}/portainer_data:/data

EOF

docker-compose -f ${DOCKER_COMPOSE_FILE} up -d

echo "You can check 'node-exporter' at http://${IP}:9000/"

```
