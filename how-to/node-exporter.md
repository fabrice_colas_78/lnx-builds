## Node-Exporter on Docker

```bash
docker run -d --name="node-exporter" --restart on-failure \
  -p 9100:9100 \
  -v /proc:/host/proc:ro \
  -v /sys:/host/sys:ro \
  -v /:/rootfs:ro \
  prom/node-exporter \
  --path.procfs=/host/proc \
  --path.sysfs=/host/sys \
  --collector.filesystem.ignored-mount-points \
  "^/(rootfs/)?(dev|etc|host|proc|run|sys|volume1)(\$\$|/)"

```

## Node-Exporter with Docker-Compose

```bash
IFACE=eth0
IP=$(ip a | grep "\beth0\b" | grep inet | awk -F" " '{ print $2 }' | awk -F'/' '{ print $1 }')

CONFIG_DIR=$HOME/node-exporter

if [ ! -d $CONFIG_DIR ]; then
  mkdir -p ${CONFIG_DIR}
fi

apt install -y docker-compose

DOCKER_COMPOSE_FILE=$CONFIG_DIR/docker-compose.yml
cat >${DOCKER_COMPOSE_FILE} <<EOF
version: '3.3'

services:
  node-exporter:
    image: prom/node-exporter
    container_name: node-exporter
    restart: always
    ports:
      - "9100:9100"
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command:
      - "--path.procfs=/host/proc"
      - "--path.sysfs=/host/sys"
      - "--collector.filesystem.ignored-mount-points"
      - "^/(rootfs/)?(dev|etc|host|proc|run|sys|volume1)($$|/)"		

EOF

docker-compose -f ${DOCKER_COMPOSE_FILE} up -d

echo "You can check 'node-exporter' at http://${IP}:9100/metrics"

```

## Node-Exporter on Debian 10/11

```bash
pushd .

sudo groupadd --system node_exporter
sudo useradd -s /sbin/nologin --system -g node_exporter node_exporter

WORK_DIR=~/tmp/node-exporter

if [ -d $WORK_DIR ]; then rm -rf $WORK_DIR; fi
mkdir -p $WORK_DIR
cd $WORK_DIR

echo "Downloading and installing node-exporter..."
curl -s https://api.github.com/repos/prometheus/node_exporter/releases/latest| grep browser_download_url|grep linux-amd64|cut -d '"' -f 4|wget -qi -
tar -xvf node_exporter*.tar.gz
cd  node_exporter*/                 
sudo cp node_exporter /usr/local/bin

node_exporter --version

echo "Creating service file..."
sudo tee /etc/systemd/system/node_exporter.service <<EOF
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=default.target
EOF

echo "Registering and starting service..."
sudo systemctl daemon-reload
sudo systemctl start node_exporter
sudo systemctl enable node_exporter

echo "Checking service status..."
systemctl status node_exporter.service | grep Active

popd

```