## `Grafana` + `Prometheus` on Docker

```bash
IFACE=eth0
IP=$(ip a | grep "\beth0\b" | grep inet | awk -F" " '{ print $2 }' | awk -F'/' '{ print $1 }')

CONFIG_DIR=$HOME/prometheus
CONFIG_FILE=$CONFIG_DIR/prometheus.yml

if [ ! -d $CONFIG_DIR ]; then
  mkdir -p ${CONFIG_DIR}
fi

if [ ! -f $CONFIG_FILE ]; then
  cat>$CONFIG_FILE <<EOF
global:
  scrape_interval: 5s
  external_labels:
    monitor: 'node'
scrape_configs:
  - job_name: 'prometheus'
    static_configs:
      - targets: ['${IP}:9090'] ## IP Address of the localhost. Match the port to your container port
  - job_name: 'node-exporter'
    static_configs:
      - targets: ['${IP}:9100'] ## IP Address of the localhost
EOF
fi

DOCKER_COMPOSE_FILE=$CONFIG_DIR/docker-compose.yml
cat >${DOCKER_COMPOSE_FILE} <<EOF
version: '3.3'

services:
  prometheus:
    image: prom/prometheus
    container_name: prometheus
    restart: always
    ports:
      - '9090:9090'
    volumes:
      - '$CONFIG_DIR:/etc/prometheus'

  grafana:
    image: grafana/grafana
    container_name: grafana
    restart: always
    ports:
      - '3000:3000'

EOF

docker-compose -f ${DOCKER_COMPOSE_FILE} up -d

echo "You can connect to 'grafana' at http://${IP}:3000/"

```

## Reload Prometheus

```bash
docker-compose kill -s SIGHUP prometheus

```
## Grafana `node-exporter` dashboard

* https://grafana.com/grafana/dashboards/11074
* https://grafana.com/grafana/dashboards/1860
* https://grafana.com/grafana/dashboards/15436
* https://grafana.com/grafana/dashboards/405

